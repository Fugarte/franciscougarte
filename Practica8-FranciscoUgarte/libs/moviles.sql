CREATE DATABASE moviles;
use moviles;

CREATE TABLE moviles(
id INTEGER auto_increment primary key,
marca varchar(50),
almacenamiento int,
color varchar(80),
precio float
);

insert into moviles (marca, almacenamiento, color, precio)
values 
('Apple', 64,'gris', 231),
('Huawei', 32,'rosa', 89),
('Samsung', 264,'rojo', 123),
('Xiaomi', 8,'azul', 70),
('Sony', 16,'blanco', 110),
('Honor', 32,'negro', 60),
('LG', 64,'verde', 55),
('HP', 16,'amarillo', 322),
('RealMe', 8,'plata', 23),
('Apple', 264,'morado', 564);