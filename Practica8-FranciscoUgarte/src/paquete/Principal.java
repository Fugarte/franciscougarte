package paquete;
import java.sql.SQLException;
import java.util.Scanner;

public class Principal {

	public static void main(String[] args) throws SQLException {
		// TODO Auto-generated method stub
		ConectandoConDatos misDatos = new ConectandoConDatos();
		Scanner in = new Scanner(System.in);
		int op=0;
		do {
			op = menu();
			switch (op) {
			case 1:
				System.out.println("Conectamos");
				misDatos.conectar();
				break;
			case 2:
				System.out.println("Inserto dato");
				System.out.println("Dame la marca del movil");
				String marca=in.nextLine();
				System.out.println("Dame el almacenamiento del movil");
				int almacenamiento=in.nextInt();
				in.nextLine();
				System.out.println("Dame el color del movil");
				String color=in.nextLine();
				System.out.println("Dame el precio del movil");
				float precio=in.nextFloat();
				misDatos.insertar(marca, almacenamiento, color, precio);
				break;
			case 3:
				System.out.println("Mostramos datos");
				misDatos.seleccionar();
				break;
			case 4:
				System.out.println("Actualizamos");
				System.out.println("Dame la marca del movil");
				marca=in.nextLine();
				System.out.println("Dame el almacenamiento del movil");
				almacenamiento=in.nextInt();
				marca=in.nextLine();
				System.out.println("Dame el color del movil");
				color=in.nextLine();
				System.out.println("Dame el precio del movil");
				precio=in.nextFloat();
				misDatos.actualizar(marca, almacenamiento, color, precio);
				break;
			case 5:
				in.nextLine();
				System.out.println("Eliminamos datos");
				System.out.println("Dame la marca del movil");
				marca=in.nextLine();
				misDatos.eliminar(marca);

			case 6:
				misDatos.desconectar();
				break;
			}
		} while (op < 7);
		in.close();
	}

	public static int menu() {
		@SuppressWarnings("resource")
		Scanner input = new Scanner(System.in);
		int opcion = 0;
		boolean error = false;
		do {
			try {
				System.out.println("MENU");
				System.out.println("1.- Conectar con la base de datos");
				System.out.println("2.- Insertar dato");
				System.out.println("3.- Mostrar datos");
				System.out.println("4.- Actualizar");
				System.out.println("5.- Eliminar por marca");
				System.out.println("6.- Desconectar");
				System.out.println("7.- Salir");
				System.out.println(" ");
				System.out.println("Elegir opcion");
				opcion = input.nextInt();
				if (opcion < 1 || opcion > 7) {
					System.out.println("Error, el valor debe estar entre 1 y 7");
					error = true;
				} else {
					error = false;
				}
			} catch (NumberFormatException e) {
				System.out.println("Error, debes introducir un numero");
				input.nextLine();
				error = true;
			} catch (Exception e) {
				System.out.println("Error de acceso a la informacion del teclado");
				System.out.println("Comprueba tu conexion al teclado");
				System.exit(0);
			}
		} while (error);
		return opcion;
	}
	
}