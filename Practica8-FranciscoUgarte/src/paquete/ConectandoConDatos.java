package paquete;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class ConectandoConDatos {

	private Connection conexion = null;
	PreparedStatement sentencia = null;

	public void conectar() {
		try {
			String servidor = "jdbc:mysql://localhost:3306/";
			String bbdd = "moviles";
			String user = "root";
			String password = "";

			conexion = DriverManager.getConnection(servidor + bbdd, user, password);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public void seleccionar() {
		String sentenciaSql = "SELECT * FROM moviles";
		try {
			sentencia = conexion.prepareStatement(sentenciaSql);

			ResultSet resultado = sentencia.executeQuery();
			// mostramos los datos
			while (resultado.next()) {
				System.out.println(resultado.getString(1) + ", " + resultado.getString(2) + ", "
						+ resultado.getInt(3) + ", " + resultado.getString(4) + ", " + resultado.getFloat(5));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			// nos aseguramos de cerrar los recursos abiertos
			if (sentencia != null) {
				try {
					sentencia.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}

	public void insertar(String marca, int almacenamiento, String color, float precio) {
		try {
			String sentenciaSql = "INSERT INTO moviles(marca, almacenamiento, color, precio) " + "values(?,?,?,?)";
			PreparedStatement sentencia;

			sentencia = conexion.prepareStatement(sentenciaSql);
			sentencia.setString(1, marca);
			sentencia.setInt(2, almacenamiento);
			sentencia.setString(3, color);
			sentencia.setFloat(4, precio);
			sentencia.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			// nos aseguramos de cerrar los recursos abiertos
			if (sentencia != null) {
				try {
					sentencia.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}

	public void actualizar(String marca, int almacenamiento, String color, float precio) {
		try {
			String sentenciaSql = "UPDATE moviles set " + "almacenamiento=?, color=?, precio=? WHERE marca=?";
			PreparedStatement sentencia;

			sentencia = conexion.prepareStatement(sentenciaSql);
			sentencia.setInt(1, almacenamiento);
			sentencia.setString(2, color);
			sentencia.setFloat(3, precio);
			sentencia.setString(4, marca);
			sentencia.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			// nos aseguramos de cerrar los recursos abiertos
			if (sentencia != null) {
				try {
					sentencia.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}

	public void eliminar(String marca) {
		try {
			String sentenciaSql = "DELETE FROM moviles WHERE marca=?";
			PreparedStatement sentencia;

			sentencia = conexion.prepareStatement(sentenciaSql);
			sentencia.setString(1, marca);
			sentencia.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			// nos aseguramos de cerrar los recursos abiertos
			if (sentencia != null) {
				try {
					sentencia.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}

	public void desconectar() throws SQLException {
		sentencia.close();
	}

}
